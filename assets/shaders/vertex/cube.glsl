// Vertex Shader
#version 460 core
layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec2 aTexCoords;

out vec2 fTexCoords;

uniform mat4 uTransform;
uniform mat4 uProjection;
uniform mat4 uView;

void main()
{
    fTexCoords = vec2(aTexCoords.x, 1.0 - aTexCoords.y);

    vec4 pos = uProjection * uView * (uTransform * vec4(aPosition, 1.0));

    gl_Position = pos;
}
