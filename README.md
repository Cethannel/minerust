<h1 align="center">
  <br>
  Minerust
  <br>
</h1>

<h4 align="center">A clone of minecraft written in rust.</h4>

<!--
<p align="center">
  <a href="https://badge.fury.io/js/electron-markdownify">
    <img src="https://badge.fury.io/js/electron-markdownify.svg"
         alt="Gitter">
  </a>
  <a href="https://gitter.im/amitmerchant1990/electron-markdownify"><img src="https://badges.gitter.im/amitmerchant1990/electron-markdownify.svg"></a>
  <a href="https://saythanks.io/to/bullredeyes@gmail.com">
      <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
  </a>
  <a href="https://www.paypal.me/AmitMerchant">
    <img src="https://img.shields.io/badge/$-donate-ff69b4.svg?maxAge=2592000&amp;style=flat">
  </a>
</p>
-->

<p align="center">
  <a href="#key-features">Key Features</a> •
  <a href="#how-to-use">How To Use</a> •
  <a href="#download">Download</a> •
  <a href="#credits">Credits</a> •
  <a href="#related">Related</a> •
  <a href="#license">License</a>
</p>

## Key Features

* Can draw squares to the screen!!!!
  - Can also move these or run shaders on them.
* Async updating of logic (so that movement/physics are not tied to framerate).
* Uses opengl.
* Full screen mode
* Cross platform
  - Windows, macOS and Linux ready (Not tested on anything other than linux).

## How To Use

To clone and run this application, you'll need [Git](https://git-scm.com) and 
[Rust](https://www.rust-lang.org/tools/install) (recommended to install with
[rustup](https://rustup.rs/)) installed on your computer. From your command line:

```bash
# Clone this repository
$ git clone https://gitlab.com/Cethannel/minerust.git

# Go into the repository
$ cd minerust

# Run the app (See bellow for different run options)
$ cargo run
```

### Build modes

```bash

# CURRENTLY NOT SUPPORTED

```

## Download

Currently no releases so compiling from source is required.

## Credits

This software uses the following open source packages:

- [Rust](https://www.rust-lang.org/)
  - [gl46](https://github.com/Lokathor/gl46)
  - [glam](https://github.com/bitshifter/glam-rs)
  - [glfw](https://github.com/PistonDevelopers/glfw-rs)
  - [memoffset](https://github.com/Gilnaa/memoffset)

## Related

This is based off of [this](https://github.com/codingminecraft/MinecraftCloneForYoutube)
project which is a minecraft clone by [GamesWithGabe](https://www.youtube.com/c/GamesWithGabe)
for [this](https://www.youtube.com/playlist?list=PLtrSb4XxIVbodGYZZVzC1PAZfwckrXp_X)
series.

## Support

Currently do not have this set up.

## Licence

GPL
