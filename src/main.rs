mod challanges;
mod file;
mod lib;
mod shader;

use challanges::cubes::{Cubes, CubesData};
use glutin::dpi::LogicalSize;
use glutin::event::{ElementState, Event, KeyboardInput, WindowEvent};
use glutin::event_loop::{ControlFlow, EventLoop};
//use glutin::platform::unix::x11::ffi::Window;
use glutin::window::{Fullscreen, WindowBuilder};
use minerust::frame_time;
use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};
use std::ffi::{c_void, CStr};
use std::sync::mpsc::Receiver;
use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::{env, thread, time::Instant};

use crate::lib::{WINDOW_HEIGHT, WINDOW_WIDTH};

struct Flags {
    fullscreen: bool,
}

fn main() {
    let flags = parse_flags();

    let event_loop = EventLoop::new();

    let wb = WindowBuilder::new()
        .with_title("Minecraft Clone")
        .with_inner_size(LogicalSize::new(WINDOW_WIDTH, WINDOW_HEIGHT));

    let window_context = glutin::ContextBuilder::new()
        .with_gl_profile(glutin::GlProfile::Core)
        .with_gl(glutin::GlRequest::Specific(glutin::Api::OpenGl, (4, 6)))
        .build_windowed(wb, &event_loop)
        .unwrap();

    let window_context = unsafe { window_context.make_current().unwrap() };

    let gl = unsafe {
        gl46::GlFns::load_from(&|u8_ptr| {
            window_context.get_proc_address(
                CStr::from_ptr(u8_ptr.cast())
                    .to_str()
                    .expect("Failed to get proc address"),
            )
        })
        .expect("Failed to load buffers")
    };

    let data_source = CubesData::new();

    let data = Arc::new(Mutex::new(data_source));

    {
        let async_data = Arc::clone(&data);
        thread::spawn(move || {
            let mut time;
            loop {
                time = Instant::now();
                let mut data = async_data.lock().unwrap();
                if data.should_exit() {
                    break;
                }
                data.update();
                drop(data);
                if time.elapsed() < frame_time {
                    let time_left = frame_time - time.elapsed();
                    thread::sleep(time_left);
                } else {
                    println!(
                        "Tick took too long: {}",
                        time.elapsed().as_millis()
                    );
                }
            }
        });
    }

    let (width, height) = window_context.window().inner_size().into();
    unsafe {
        gl.Enable(gl46::GL_DEBUG_OUTPUT);
        gl.DebugMessageCallback(
            Some(error_message_callback),
            0 as *const c_void,
        );
        gl.Enable(gl46::GL_DEPTH_TEST);
        gl.Viewport(0, 0, width, height);
    }

    let wc = Arc::new(window_context);

    let gl = Arc::new(gl);

    game_loop(wc, &data, event_loop, gl);
}

fn game_loop(
    window_context: Arc<glutin::WindowedContext<glutin::PossiblyCurrent>>,
    data: &Arc<Mutex<CubesData>>,
    event_loop: glutin::event_loop::EventLoop<()>,
    gl: Arc<gl46::GlFns>,
) {
    let cubes = Cubes::new(gl.clone(), window_context.window());

    let mut dt: f32 = 0.016;
    let mut frame_start: f32 = 0.0;
    let start_time = Instant::now();

    let mut window_resized = false;

    let data = data.clone();

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                println!("The close button was pressed; stopping");
                *control_flow = ControlFlow::Exit
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(_),
                ..
            } => {
                window_resized = true;
            }
            Event::MainEventsCleared => {
                if window_resized {
                    let (width, height) =
                        window_context.window().inner_size().into();
                    unsafe {
                        gl.Viewport(0, 0, width, height);
                    }
                }
                let data = data.lock().unwrap();
                let time = start_time.elapsed().as_secs() as f32;
                dt = time - frame_start;
                frame_start = start_time.elapsed().as_secs() as f32;

                unsafe {
                    gl.ClearColor(
                        250.0 / 255.0,
                        119.0 / 255.0,
                        110.0 / 255.0,
                        1.0,
                    );
                    gl.Clear(
                        gl46::GL_COLOR_BUFFER_BIT | gl46::GL_DEPTH_BUFFER_BIT,
                    );
                }

                cubes.update(&data);

                window_context
                    .swap_buffers()
                    .expect("Failed to swap buffers");

                drop(data);
                //std::thread::sleep(Duration::from_millis(1));
            }
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state,
                                virtual_keycode,
                                ..
                            },
                        ..
                    },
                ..
            } => {
                let press = match state {
                    ElementState::Pressed => true,
                    ElementState::Released => false,
                };
                match virtual_keycode {
                    _ => {}
                }
            }
            _ => (),
        }
    });
}

fn parse_flags() -> Flags {
    let mut out = Flags { fullscreen: false };
    for argument in env::args() {
        // If "-folder" detected, set flag bool.
        if argument == "-fullscreen" {
            out.fullscreen = true;
            continue;
        }
    }
    out
}

unsafe extern "system" fn error_message_callback(
    source: gl46::GLenum,
    type_: gl46::GLenum,
    id: u32,
    severity: gl46::GLenum,
    length: i32,
    message: *const u8,
    userParam: *const c_void,
) {
    let message = CStr::from_ptr(message as *const i8);
    println!(
        "GL CALLBACK: **GL ERROR** type = {:?}, severity = {:?}, message = {}",
        type_,
        severity,
        message.to_str().unwrap()
    );
}
