use std::{
    collections::HashMap,
    ffi::{CStr, CString},
    fs,
    os::raw::c_char, sync::Arc,
};

#[derive(Clone)]
enum ShaderType {
    Vetex,
    Fragment,
}

impl ShaderType {
    fn value(&self) -> gl46::GLenum {
        match self {
            ShaderType::Vetex => gl46::GL_VERTEX_SHADER,
            ShaderType::Fragment => gl46::GL_FRAGMENT_SHADER,
        }
    }
}

#[derive(Clone)]
struct Shader {
    filepath: String,
    shader_id: u32,
    shader_type: ShaderType,
    gl: Arc<gl46::GlFns>,
}

impl Shader {
    fn new(
        shader_type: ShaderType,
        filepath: String,
        gl: Arc<gl46::GlFns>,
    ) -> Self {
        Shader {
            filepath,
            shader_id: 0,
            shader_type,
            gl,
        }
    }

    unsafe fn compile(&mut self) -> Option<&mut Self> {
        #[cfg(debug_assertions)]
        println!("Reading: {}", self.filepath.clone());
        let shader = CString::new(
            fs::read_to_string(self.filepath.clone())
                .expect("Failed to read shader"),
        )
        .unwrap();

        self.shader_id = self.gl.CreateShader(self.shader_type.value());

        self.gl.ShaderSource(
            self.shader_id,
            1,
            &shader.as_ptr() as *const *const i8 as *const *const u8,
            0 as *const _,
        );

        self.gl.CompileShader(self.shader_id);

        #[cfg(debug_assertions)]
        println!("Compiled shader");

        let mut is_compiled: i32 = 0;

        self.gl.GetShaderiv(
            self.shader_id,
            gl46::GL_COMPILE_STATUS,
            &mut is_compiled as *mut i32,
        );

        if is_compiled == 0 {
            let mut max_len: i32 = 0;
            self.gl.GetShaderiv(
                self.shader_id,
                gl46::GL_INFO_LOG_LENGTH,
                &mut max_len as *mut i32,
            );

            let mut infolog: Vec<u8> = Vec::with_capacity(max_len as usize);
            for _ in 0..max_len {
                infolog.push(b'\0');
            }

            self.gl.GetShaderInfoLog(
                self.shader_id,
                max_len,
                &mut max_len as *mut i32,
                infolog.as_mut_ptr(),
            );

            self.gl.DeleteShader(self.shader_id);

            println!(
                "Shader compilation failed: \n{}",
                CString::from_vec_unchecked(infolog).to_str().unwrap()
            );

            return None;
        }

        Some(self)
    }

    unsafe fn destroy(&self) {
        self.gl.DeleteShader(self.shader_id);
    }
}

pub enum UploadTypes {
    Vec4(glam::f32::Vec4),
    Vec3(glam::f32::Vec3),
    Vec2(glam::f32::Vec2),
    IVec4(glam::i32::IVec4),
    IVec3(glam::i32::IVec3),
    IVec2(glam::i32::IVec2),
    Float(f32),
    Int(i32),
    UInt(u32),
    Mat4(glam::f32::Mat4),
    Mat3(glam::f32::Mat3),
    IntArray(Vec<i32>),
    Bool(bool),
}

#[derive(Clone)]
pub struct ShaderProgram {
    program_id: u32,
    variables: HashMap<String, i32>,
    gl: Arc<gl46::GlFns>,
}

impl ShaderProgram {
    pub fn new(gl: Arc<gl46::GlFns>) -> Self {
        ShaderProgram {
            program_id: 0,
            variables: HashMap::new(),
            gl,
        }
    }

    pub fn compile_and_link(
        &mut self,
        vertex_shader_file: &str,
        fragment_shader_file: &str,
    ) -> Option<&mut Self> {
        let mut vert_shader = Shader::new(
            ShaderType::Vetex,
            vertex_shader_file.to_string(),
            self.gl.clone(),
        );
        let mut frag_shader = Shader::new(
            ShaderType::Fragment,
            fragment_shader_file.to_string(),
            self.gl.clone(),
        );
        unsafe {
            vert_shader
                .compile()
                .expect("Failed to compile vertex shader");
            frag_shader
                .compile()
                .expect("Failed to compile fragment shader");

            self.program_id = self.gl.CreateProgram();

            self.gl.AttachShader(self.program_id, vert_shader.shader_id);
            self.gl.AttachShader(self.program_id, frag_shader.shader_id);

            self.gl.LinkProgram(self.program_id);

            let mut is_linked: i32 = 0;
            self.gl.GetProgramiv(
                self.program_id,
                gl46::GL_LINK_STATUS,
                &mut is_linked as *mut i32,
            );
            if is_linked == 0 {
                let mut max_len: i32 = 0;
                self.gl.GetProgramiv(
                    self.program_id,
                    gl46::GL_INFO_LOG_LENGTH,
                    &mut max_len as *mut i32,
                );

                let mut info_log: Vec<u8> = vec![b'\0'; 512];
                //let info_log = CString::from_vec_unchecked(info_vec);

                self.gl.GetProgramInfoLog(
                    self.program_id,
                    max_len,
                    &mut max_len as *mut i32,
                    info_log.as_mut_ptr() as *mut u8,
                );

                println!("Got log");

                self.gl.DeleteProgram(self.program_id);
                self.gl.DeleteShader(vert_shader.shader_id);
                self.gl.DeleteShader(frag_shader.shader_id);

                println!(
                    "Shader linking failed: \n{:?}",
                    CString::from_vec_unchecked(info_log)
                );

                return None;
            }

            self.gl.DetachShader(self.program_id, vert_shader.shader_id);
            self.gl.DetachShader(self.program_id, frag_shader.shader_id);
            vert_shader.destroy();
            frag_shader.destroy();

            let mut num_uniforms: i32 = 0;
            self.gl.GetProgramiv(
                self.program_id,
                gl46::GL_ACTIVE_UNIFORMS,
                &mut num_uniforms as *mut i32,
            );

            let mut max_len = 0;
            self.gl.GetProgramiv(
                self.program_id,
                gl46::GL_ACTIVE_UNIFORM_MAX_LENGTH,
                &mut max_len as *mut i32,
            );
            if num_uniforms > 0 && max_len > 0 {
                // max_len += 1;
                let mut buf: Vec<u8> = Vec::with_capacity(max_len as usize);
                buf.set_len(max_len as usize);
                // println!("Number of uniforms: {}", num_uniforms);
                // println!("Max length: {}", max_len);

                for i in 0..num_uniforms as u32 {
                    buf = buf.iter().map(|_| b'\0' as u8).collect();
                    let mut length: i32 = 0;
                    let mut size: i32 = 0;
                    let mut data_type: gl46::GLenum = gl46::GL_MAX;
                    self.gl.GetActiveUniform(
                        self.program_id,
                        i,
                        max_len,
                        &mut length,
                        &mut size,
                        &mut data_type,
                        &mut buf[0] as *mut u8,
                        // &mut buf_test as *mut c_char
                    );
                    let buf_clone = buf.clone();
                    let val =
                        CStr::from_ptr(&buf_clone[0] as *const u8 as *const i8)
                            .to_str()
                            .expect("HERE")
                            .to_string();
                    // println!("{}", val);
                    let var_location = self.gl.GetUniformLocation(
                        self.program_id,
                        &mut buf[0] as *mut u8,
                    );
                    self.variables.insert(
                        // String::from_utf8(buf.iter().map(|a| *a as u8).collect()).unwrap(),
                        // CString::from_vec_unchecked(buf.iter().map(|a| *a as u8).collect()).into_string().unwrap(),
                        val,
                        var_location,
                    );
                }
                // println!("{:?}", self.variables);
            }

            // self.add_variable("time".to_string());
            // self.add_variable("mouse".to_string());
            // self.add_variable("resolution".to_string());
        }

        #[cfg(debug_assertions)]
        println!(
            "Shader compilation and linking succeded <Vertex:{}>:<Fragment:{}>",
            vertex_shader_file, fragment_shader_file
        );
        Some(self)
    }

    // fn add_variable(&mut self, name: String) {
    //     let name_c = CString::new(name.clone()).unwrap();
    //     let location = unsafe {gl::GetUniformLocation(self.program_id, name_c.as_ptr())};
    //     self.variables.insert(name, location);
    // }

    pub fn bind(&self) {
        self.gl.UseProgram(self.program_id)
    }

    pub fn unbind(&self) {
        self.gl.UseProgram(0)
    }

    pub fn destroy(&mut self) {
        if self.program_id != u32::MAX {
            self.gl.DeleteProgram(self.program_id);
            self.program_id = u32::MAX;
        }
    }

    pub fn upload(&self, var_name: &str, value: UploadTypes) {
        let var_name = var_name.to_string();
        unsafe {
            match value {
                UploadTypes::Vec4(val) => self.gl.Uniform4f(
                    self.get_val(var_name),
                    val.x,
                    val.y,
                    val.z,
                    val.w,
                ),
                UploadTypes::Vec3(val) => self.gl.Uniform3f(
                    self.get_val(var_name),
                    val.x,
                    val.y,
                    val.z,
                ),
                UploadTypes::Vec2(val) => {
                    self.gl.Uniform2f(self.get_val(var_name), val.x, val.y)
                }
                UploadTypes::IVec4(val) => self.gl.Uniform4i(
                    self.get_val(var_name),
                    val.x,
                    val.y,
                    val.z,
                    val.w,
                ),
                UploadTypes::IVec3(val) => self.gl.Uniform3i(
                    self.get_val(var_name),
                    val.x,
                    val.y,
                    val.z,
                ),
                UploadTypes::IVec2(val) => {
                    self.gl.Uniform2i(self.get_val(var_name), val.x, val.y)
                }
                UploadTypes::Float(val) => {
                    self.gl.Uniform1f(self.get_val(var_name), val)
                }
                UploadTypes::Int(val) => {
                    self.gl.Uniform1i(self.get_val(var_name), val)
                }
                UploadTypes::UInt(val) => {
                    self.gl.Uniform1ui(self.get_val(var_name), val)
                }
                UploadTypes::Mat4(val) => self.gl.UniformMatrix4fv(
                    self.get_val(var_name),
                    1,
                    0,
                    &val.to_cols_array()[0] as *const f32,
                ),
                UploadTypes::Mat3(val) => self.gl.UniformMatrix4fv(
                    self.get_val(var_name),
                    1,
                    0,
                    &val.to_cols_array()[0] as *const f32,
                ),
                UploadTypes::IntArray(val) => self.gl.Uniform1iv(
                    self.get_val(var_name),
                    val.len() as i32,
                    val.as_ptr(),
                ),
                UploadTypes::Bool(val) => {
                    self.gl.Uniform1i(self.get_val(var_name), val.into())
                }
            }
        }
    }

    pub fn clear_shader_vars(&mut self) {
        self.variables.clear()
    }

    pub fn print_vars(&self) {
        println!("Variables");
        for (var, index) in self.variables.clone() {
            println!("{}: {}", var, index);
            if var == "uProjection".to_string() {
                println!("Found it");
            }
        }
    }

    fn get_val(&self, var_name: String) -> i32 {
        *self.variables.get(&var_name).expect(
            format!("No variable with this name: {}", var_name).as_str(),
        )
    }
}
