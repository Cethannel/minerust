use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

use minecraft::WINDOW_WIDTH;

use crate::{
    challanges::shader_examples::{bump_shader_location, ShaderExamples},
    shader::{ShaderProgram, UploadTypes},
    WINDOW_HEIGHT,
};

#[derive(Clone)]
pub enum CallbackArgTypes {
    ShaderExamples(Arc<Mutex<ShaderExamples>>),
}

//pub enum CallbackTypes

pub type CallbackFn<T> = fn(T);

struct Mouse {
    x: f32,
    y: f32,
    scroll_x: f32,
    scroll_y: f32,
}

pub struct Input<'a> {
    pub glfw: &'a mut glfw::Glfw,
    pub window: &'a mut glfw::Window,
    mouse: Mouse,
}

impl<'a> Input<'a> {
    pub fn new(glfw: &'a mut glfw::Glfw, window: &'a mut glfw::Window) -> Input<'a> {
        Input {
            glfw,
            window,
            mouse: Mouse {
                x: 0.0,
                y: 0.0,
                scroll_x: 0.0,
                scroll_y: 0.0,
            },
        }
    }

    pub fn my_key_callback(
        &mut self,
        key: glfw::Key,
        _scancode: glfw::Scancode,
        action: glfw::Action,
        _mods: glfw::Modifiers,
        callback_args: &HashMap<String, Vec<CallbackArgTypes>>,
    ) {
        if key == glfw::Key::E && action == glfw::Action::Press {
            println!("pressed E key");
        }
        match action {
            glfw::Action::Press => match key {
                glfw::Key::E => {
                    println!("pressed E key");
                }
                glfw::Key::Escape => {
                    self.window.set_should_close(true);
                }
                glfw::Key::F11 => {
                    let mut fullscreen = false;
                    self.window.with_window_mode(|mode| match mode {
                        glfw::WindowMode::FullScreen(_) => fullscreen = false,
                        glfw::WindowMode::Windowed => fullscreen = true,
                    });
                    match fullscreen {
                        false => {
                            self.window.set_monitor(
                                glfw::WindowMode::Windowed,
                                0,
                                0,
                                WINDOW_WIDTH,
                                WINDOW_HEIGHT,
                                None,
                            );
                        }
                        true => {
                            self.glfw.with_primary_monitor(|_, m| {
                                self.window.set_monitor(
                                    m.map_or(glfw::WindowMode::Windowed, |m| {
                                        glfw::WindowMode::FullScreen(m)
                                    }),
                                    0,
                                    0,
                                    WINDOW_WIDTH,
                                    WINDOW_HEIGHT,
                                    None,
                                )
                            });
                        }
                    }
                }
                glfw::Key::Right => {
                    let callback = &callback_args["shader_bump"];
                    bump_shader_location(callback.first().unwrap().clone());
                }
                _ => {}
            },
            _ => {}
        }
    }

    pub fn my_cursor_move_callback(&mut self, x: f64, y: f64) {
        self.mouse.x = x as f32;
        self.mouse.y = y as f32;
    }

    pub fn my_scroll_callback(&mut self, x: f64, y: f64) {
        self.mouse.scroll_x = x as f32;
        self.mouse.scroll_y = y as f32;
    }
}
