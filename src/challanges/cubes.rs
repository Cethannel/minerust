use std::{ffi::c_void, mem::size_of, sync::Arc};

use image::{ColorType, EncodableLayout, GenericImageView};
use memoffset::offset_of;
use rand::Rng;

use crate::shader;

static CUBE_ELEMENTS: [usize; 36] = [
    // Each set of 6 indices represents one quad
    1, 0, 2, 3, 1, 2, // Front face
    5, 1, 3, 7, 5, 3, // Right face
    7, 6, 4, 5, 7, 4, // Back face
    0, 4, 6, 2, 0, 6, // Left face
    5, 4, 0, 1, 5, 0, // Top face
    3, 2, 6, 7, 3, 6, // Bottom face
];

#[derive(Clone, Copy, Default, Debug)]
struct Vertex {
    position: glam::Vec3,
    tex_coords: glam::Vec2,
}

#[derive(Debug)]
struct Texture {
    path: String,
    texture_id: u32,
    width: i32,
    height: i32,
    num_channels: i32,
}

impl Texture {
    pub fn new(path: String, pixilated: bool, gl: Arc<gl46::GlFns>) -> Self {
        let mut texture = Texture {
            path,
            texture_id: 0,
            width: 0,
            height: 0,
            num_channels: 0,
        };

        let image = image::io::Reader::open(&texture.path)
            .expect("Incorrect texture path")
            .decode()
            .expect("Failed to decode image");

        texture.width = image.width() as i32;
        texture.height = image.height() as i32;

        match image.color() {
            ColorType::Rgb8 | ColorType::Rgb16 | ColorType::Rgb32F => {
                texture.num_channels = 3;
            }
            ColorType::Rgba8 | ColorType::Rgba16 | ColorType::Rgba32F => {
                texture.num_channels = 4;
            }
            _ => {
                println!(
                    "Unknown image type: {:?} in image: {}",
                    image.color(),
                    texture.path
                );
                return Texture {
                    path: "".to_string(),
                    texture_id: 0,
                    width: -1,
                    height: -1,
                    num_channels: -1,
                };
            }
        }

        let (texture_format, internal_format, pixels) =
            if texture.num_channels == 3 {
                (
                    gl46::GL_RGB,
                    gl46::GL_RGB32F,
                    image.as_rgb8().expect("Failed to conver to rgb8").as_bytes().to_vec(),
                )
            } else {
                (
                    gl46::GL_RGBA,
                    gl46::GL_RGBA32F,
                    image.as_rgba8().expect("Failed to conver to rgba8").as_bytes().to_vec(),
                )
            };

        let filter = match pixilated {
            true => gl46::GL_NEAREST.0 as i32,
            false => gl46::GL_LINEAR.0 as i32,
        };

        #[cfg(debug_assertions)]
        println!("Creating texture");

        #[cfg(debug_assertions)]
        println!("{:?}", texture);

        unsafe {
            gl.GenTextures(1, &mut texture.texture_id as *mut u32);
            gl.BindTexture(gl46::GL_TEXTURE_2D, texture.texture_id);

            gl.TexParameteri(
                gl46::GL_TEXTURE_2D,
                gl46::GL_TEXTURE_WRAP_S,
                gl46::GL_REPEAT.0 as i32,
            );
            gl.TexParameteri(
                gl46::GL_TEXTURE_2D,
                gl46::GL_TEXTURE_WRAP_T,
                gl46::GL_REPEAT.0 as i32,
            );
            gl.TexParameteri(
                gl46::GL_TEXTURE_2D,
                gl46::GL_TEXTURE_MIN_FILTER,
                filter,
            );
            gl.TexParameteri(
                gl46::GL_TEXTURE_2D,
                gl46::GL_TEXTURE_MAG_FILTER,
                filter,
            );

            gl.TexImage2D(
                gl46::GL_TEXTURE_2D,
                0,
                internal_format.0 as i32,
                texture.width,
                texture.height,
                0,
                texture_format,
                gl46::GL_UNSIGNED_BYTE,
                pixels.as_ptr() as *const c_void
            );
        }

        texture
    }
}

struct Cube {
    vao: u32,
    vbo: u32,
    verteces: [Vertex; 36],
    gl: Arc<gl46::GlFns>,
}

impl Cube {
    fn new(gl: Arc<gl46::GlFns>) -> Self {
        let mut vao = 0;
        let mut vbo = 0;
        let mut verteces = [Vertex::default(); 36];
        let cube_vertices: [glam::Vec3; 8] = [
            glam::vec3(-0.5, 0.5, 0.5),
            glam::vec3(0.5, 0.5, 0.5),
            glam::vec3(-0.5, -0.5, 0.5),
            glam::vec3(0.5, -0.5, 0.5),
            glam::vec3(-0.5, 0.5, -0.5),
            glam::vec3(0.5, 0.5, -0.5),
            glam::vec3(-0.5, -0.5, -0.5),
            glam::vec3(0.5, -0.5, -0.5),
        ];

        let tex_coords: [glam::Vec2; 6] = [
            glam::vec2(1.0, 1.0),
            glam::vec2(0.0, 1.0),
            glam::vec2(0.0, 0.0),
            glam::vec2(1.0, 0.0),
            glam::vec2(1.0, 1.0),
            glam::vec2(0.0, 0.0),
        ];

        for index in 0..(CUBE_ELEMENTS.len()) {
            verteces[index].position = cube_vertices[CUBE_ELEMENTS[index]];
            verteces[index].tex_coords = tex_coords[index % 6];
        }

        unsafe {
            gl.GenVertexArrays(1, &mut vao as *mut u32);
        }
        gl.BindVertexArray(vao);

        unsafe {
            gl.GenBuffers(1, &mut vbo as *mut u32);
            gl.BindBuffer(gl46::GL_ARRAY_BUFFER, vbo);
            gl.BufferData(
                gl46::GL_ARRAY_BUFFER,
                (size_of::<Vertex>() * verteces.len()) as isize,
                verteces.as_ptr() as *const c_void,
                gl46::GL_STATIC_DRAW,
            );

            gl.VertexAttribPointer(
                0,
                3,
                gl46::GL_FLOAT,
                0,
                size_of::<Vertex>() as i32,
                offset_of!(Vertex, position) as *const c_void, //(size_of::<f32>() * 4) as *const c_void,
            );
            gl.EnableVertexAttribArray(0);

            gl.VertexAttribPointer(
                1,
                2,
                gl46::GL_FLOAT,
                0,
                size_of::<Vertex>() as i32,
                offset_of!(Vertex, tex_coords) as *const c_void, //(size_of::<f32>() * 4) as *const c_void,
            );
            gl.EnableVertexAttribArray(1);
        }

        Cube {
            gl,
            vao,
            vbo,
            verteces,
        }
    }

    fn draw(
        &self,
        position: glam::Vec3,
        shader: &shader::ShaderProgram,
        texture: &Texture,
    ) {
        let transform = glam::Mat4::from_translation(position);
        shader.upload("uTransform", shader::UploadTypes::Mat4(transform));

        let texture_slot = 0;
        unsafe {
            self.gl.ActiveTexture(gl46::GLenum(
                gl46::GL_TEXTURE0.0 + texture_slot,
            ));
            self.gl.BindTexture(gl46::GL_TEXTURE_2D, texture.texture_id);
        }

        shader
            .upload("uTexture", shader::UploadTypes::Int(texture_slot as i32));

        self.gl.BindVertexArray(self.vao);
        unsafe {
            self.gl.DrawArrays(
                gl46::GL_TRIANGLES,
                0,
                self.verteces.len() as i32,
            );
        }
    }
}

impl Drop for Cube {
    fn drop(&mut self) {
        unsafe {
            self.gl.DeleteBuffers(1, &self.vbo as *const u32);
            self.gl.DeleteVertexArrays(1, &self.vao as *const u32);
        }
    }
}

pub struct Cubes {
    gl: Arc<gl46::GlFns>,
    default_cube: Cube,
    shader: shader::ShaderProgram,
    projection: glam::Mat4,
    cube_positions: Vec<glam::Vec3>,
    textures: Vec<Texture>,
    cube_textures: Vec<usize>,
}

impl Cubes {
    pub fn new(gl: Arc<gl46::GlFns>, window: &glutin::window::Window) -> Self {
        let mut shader = shader::ShaderProgram::new(gl.clone());
        shader.compile_and_link(
            "assets/shaders/vertex/cube.glsl",
            "assets/shaders/fragment/cube.glsl",
        );
        shader.bind();
        let default_cube = Cube::new(gl.clone());

        let mut textures = Vec::new();

        for file in std::fs::read_dir("assets/images").unwrap() {
            let file_path = file.expect("Failed to unwrap file").path().to_str()
                .expect("Failed to convert path to str").to_string();
            if !file_path.contains("normal") {
                textures.push(Texture::new(
                    file_path,
                    true,
                    gl.clone(),
                ));
            }
        }

        let (width, height): (i32, i32) = window.inner_size().into();
        println!("The widht is: {}", width);
        let window_aspect = width as f32 / height as f32;
        let fov = 90.0;
        let z_near = 0.1;
        let z_far = 10000.0;
        let projection =
            glam::Mat4::perspective_lh(fov, window_aspect, z_near, z_far);

        let mut cube_positions = Vec::new();

        let mut rng = rand::thread_rng();

        let mut cube_textures = Vec::new();
        for i in -8..9 {
            for j in -8..9 {
                for k in 0..3 {
                    cube_positions.push(glam::vec3(i as f32, k as f32, j as f32));
                    cube_textures.push(rng.gen_range(0..textures.len()));
                }
            }
        }

        for i in 0..10 {
            cube_positions.push(glam::vec3(
                rng.gen_range(-5..6) as f32,
                3.0,
                rng.gen_range(-5..6) as f32,
            ));
            cube_textures.push(rng.gen_range(0..textures.len()));
        }

        Self {
            default_cube,
            gl,
            shader,
            projection,
            cube_positions,
            textures,
            cube_textures
        }
    }

    pub fn update(&self, data: &CubesData) {
        self.shader.bind();

        let eye = glam::vec3(
            7.0 * data.rotation.to_radians().sin(),
            4.0,
            7.0 * data.rotation.to_radians().cos(),
        );

        let center = glam::Vec3::new(0.0, 0.0, 0.0);
        let up = glam::vec3(0.0, 1.0, 0.0);
        let view = glam::Mat4::look_at_lh(eye, center, up);

        self.shader.upload("uView", shader::UploadTypes::Mat4(view));
        self.shader
            .upload("uProjection", shader::UploadTypes::Mat4(self.projection));

        for (i, pos) in self.cube_positions.iter().enumerate() {
            self.default_cube.draw(
                *pos,
                &self.shader,
                &self.textures[self.cube_textures[i]],
            );
        }
    }
}

impl Drop for Cubes {
    fn drop(&mut self) {
        self.shader.destroy();
    }
}

pub struct CubesData {
    rotation: f32,
    exit: bool,
}

impl CubesData {
    pub fn new() -> Self {
        Self {
            rotation: 45.0,
            exit: false,
        }
    }

    pub fn update(&mut self) {
        self.rotation += 1.0;
    }

    pub fn should_exit(&self) -> bool {
        self.exit
    }
}
