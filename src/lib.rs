use std::time::Duration;

use glam::f32::{Vec3, Vec2};

pub const WINDOW_WIDTH: u32 = 1280;
pub const WINDOW_HEIGHT: u32 = 720;

pub const frame_time: Duration = std::time::Duration::from_nanos(1000000000 / 60);

// #[derive(Clone, Copy, Debug)]
// pub struct Vertex {
//     pub color: Vec4,
//     pub position: Vec3,
// }

pub fn to_global_cords(cords: Vec3, resolution: Vec2) -> Vec3 {
    let xy = cords.truncate()  / resolution;
    xy.extend(cords.z)
}

#[derive(Clone, Copy)]
pub struct DefaultData {

}

impl DefaultData {
    pub fn new() -> Self {
        DefaultData {}
    }

    pub fn should_exit(self) -> bool {
        return false;
    }

    pub fn update(self) {
        println!("You should not be calling this");
    }

    pub fn update_resolution(self, _size: glam::Vec2) {
        assert!(false);
    }
}
